<?php
/**
 * Created by PhpStorm.
 * User: Валентин
 * Date: 14.10.2018
 * Time: 11:54
 */

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\grid\GridView;

$this->title = 'Логи';

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $filterModel,
    'columns' => [
        [
            'attribute' => 'ts',
            'format' => ['date', 'php:d.m.Y H:i:s'],
            'filter' => false,
        ],
        [
            'attribute' => 'type',
            'format' => 'text',
            'filter' => array_combine(range(1, 10), range(1, 10)),
        ],
        [
            'attribute' => 'message',
            'format' => 'text',
            'filter' => false,
        ],
    ],
]);